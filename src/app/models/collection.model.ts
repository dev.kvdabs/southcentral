export class Collection {
    ID: number;
    CustomerID: number;
    ReferenceNo: string;
    Date: Date | any;
    CollectionPlanId?: number;
    TotalAmountDue: number;
    TotalAmountPayable: number;
    CashAmount: number;
    TotalAmountPaid: number;
    PaymentType: number;
    Status: number;
    Details: CollectionDetail[];
    CheckPayments: ChequePayment[];
    BankTransferPayments: BankTransferPayment[]
}

export class CollectionDetail {
    InvoiceID: number;
    Amount: number;
    AmountPaid: number;
}

export class ChequePayment {
    ID: number;   
    CollectionID: number; 
    BankCode: string;
    DueDate: Date | any;
    CheckNo: string;
    Amount: number;
    IsPostDated: boolean;
    Remarks: string;
    Status: number;
    RecordStatus: number;
}

export class BankTransferPayment {
    ID: number;
    CollectionID: number; 
    TransferDate: Date | any;
    BankCode: string;
    ReferenceNo: string;
    Amount: number;
    Remarks: string;
    Status: number;
    RecordStatus: number;
}

export class CollectionPlan {
    ID: number;
    ReleaseDate: Date;
    CollectionPeriodFrom: Date;
    CollectionPeriodTo: Date;
    TotalCollectible: number;
    TotalCollected: number;
    TotalBalance: number;
    Details: any[];
    Remarks: string;
}