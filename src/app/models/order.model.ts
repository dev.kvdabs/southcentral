export class Order {
    ID: number;
    OrderDate: Date | any;
    DeliveryDate: Date | any;
    CustomerID: number;
    ModeOfPayment: number;
    OrderType: number;
    GrossTotal: number;
    NetTotal: number;
    Details: OrderItem[]
}

export class OrderItem {
    SalesOrderID: number;
    ItemID: number;
    ItemName?: string;
    Qty: number;
    Price: number;
    Amount: number;
    RequestedPrice: number;
}