export class Customer {
    ID: number;
    Name: string;
    TIN: string;
    Address: string;
    ZipCode: string;
    BusinessPhoneNo: string;
    OfficePhoneNo: string;
    MobileNo: string;
    Email: string;
    BillToAddress: string;
    ShipToAddress: string;
    PreferredPaymentMethod: string;
    ContactPerson: string;
    ContactNo: string;
    ContactEmail: string;
    AreaID: number;
    TerritoryID: number;
    CreditLines: any[];
}