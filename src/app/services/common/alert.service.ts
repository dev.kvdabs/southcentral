import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private alert: AlertController) { }

  async create(header: string, subHeader: string, inputs: any[], button: any) {
    const alert = await this.alert.create({
      header: header,
      subHeader: subHeader,
      mode: "ios",
      backdropDismiss: false,
      inputs: inputs,
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            
          }
        },
        button     
      ]
    });

    return alert;
  }

}
