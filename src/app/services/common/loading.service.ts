import { Injectable } from '@angular/core';
import { ToastController, LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  constructor(private loading: LoadingController) { }

  async create(message?: string) {
    const loader = await this.loading.create({
      mode: 'ios',
      showBackdrop: true,
      //spinner: 'dots',
      message: message
    });

    return loader;
  }
}
