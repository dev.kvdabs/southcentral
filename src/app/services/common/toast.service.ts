import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toast: ToastController) { }

  async error(message?: string, duration?: number, show: boolean = false) {
    const toast = await this.toast.create({
      mode: 'ios',
      color: 'danger',
      duration: duration || 1000,
      showCloseButton: true,
      message: message || "Unable to connect to server. Please check internet connection"
    });

    if (show) toast.present();
    else return toast;
  }

  async success(message: string, duration?: number, show: boolean = false) {
    const toast = await this.toast.create({
      mode: 'ios',
      color: 'success',
      duration: duration || 1000,
      message: message
    });

    if (show) toast.present();
    else return toast;
  }
}
