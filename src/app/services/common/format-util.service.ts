import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class FormatUtilService {

  constructor() { }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();    
  }

  formatDate(date) {
    return moment(new Date(date)).format("MM-DD-YYYY");
  }
}
