import { Injectable } from '@angular/core';
import { FormatUtilService } from './format-util.service';
import { FormGroup } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor(private formatUtil: FormatUtilService) { }

  sum(array: any[], prop: string) {
    return array.reduce( (tot, record) => {
      return tot + record[prop];
    }, 0);
  }  

  constToArray(constObj) {
    let arr = [];

    Object.entries(constObj).forEach(item => {
      arr.push({
        id: item['1'],
        name: this.formatUtil.capitalizeFirstLetter(item['0'].replace('_', ' '))
      });
    });

    return arr;
  }

  setFormValue(form: FormGroup, prop: string, value: any) {
    if (form.controls[prop]) form.controls[prop].setValue(value);
  }

  getFormValue(form: FormGroup, prop: string) {
    return form.controls[prop].value;
  }

  filter(arr, props: string[] | any, searchKey, limit: number = 0) {
    
    let count = 0;

    if (searchKey.trim()) return arr.filter(x => {

      if (limit > 0 && count == limit) return;

      // single field
      if (typeof props == "string") {
        count++;
        return (x[props]).toString().toLowerCase().includes(searchKey.toLowerCase())
      }
      
      // multiple fields
      let match = false;
      for (var i=0; i<props.length; i++) {
        match = (x[props[i]]).toString().toLowerCase().includes(searchKey.toLowerCase())
        if (match) {
          count++;
          break;
        }
      }

      return match;
    });

    return arr;
  }
}
