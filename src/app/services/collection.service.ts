import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Subject } from 'rxjs';
import { Collection } from '../models/collection.model';
import { AuthService } from './auth.service';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class CollectionService {

  private api = '';
  private _collections: any;
  private _collectionPlans: any;
  $collections = new Subject<any[]>();

  constructor(private http: HttpService, private auth: AuthService, private storage: Storage) { 
    this.api = "/collections";

    this.auth.$loggedOut.subscribe(() => this._collections = []);
  }

  async collections(cache?: boolean) {
    if (cache) return this._collections; // for searching

    let collections: any[] = await this.http.get(this.api);
    
    this._collections = collections.map(this.formatResponse);

    this.$collections.next(this._collections);
  }

  collection(id: number) {
    return this.http.get(this.api + '?id=' + id);
  }

  async add(collection: Collection) {
    try {
      const res: any = await this.http.post(this.api + '/add', collection);

      if (res.Message == "Success") {
        this._collections.push(this.formatResponse(res.Model));
        this.$collections.next(this._collections);

        return {
          success: true
        }
      }

      return {
        success: false,
        message: res.Message
      }
    } catch (error) {

      return {
        success: false
      }
    }
  }

  async update(collection: Collection) {
    try {
      const res: any = await this.http.post(this.api + '/update', collection);

      if (res.Message == "Success") {

        const index = this._collections.findIndex(x => x.ID == collection.ID);
        this._collections.splice(index, 1, this.formatResponse(res.Model));
        this.$collections.next(this._collections);

        return {
          success: true
        }
      }

      return {
        success: false,
        message: res.Message
      }
    } catch (error) {

      return {
        success: false
      }
    }
  }

  async collectionplans(cache?: boolean) {
    if (cache) return this._collectionPlans; // for searching

    const baseApi = await this.storage.get("API_URL");
    const data = await this.http.get(baseApi + 'api/collection-plan/get-list', true);

    this._collectionPlans = data.map(x => ({
      ID: x.ID,
      ReleaseDate: x.ReleaseDate,
      CollectionPeriodFrom: x.CollectionPeriodFrom,
      CollectionPeriodTo: x.CollectionPeriodTo,
      TotalCollectible: x.TotalCollectible,
      TotalCollected: x.TotalCollected,
      TotalBalance: x.TotalBalance,
      Remarks: x.Remarks,
      InvoiceCount: x.Details ? x.Details.length : 0,
      Details: x.Details ? x.Details.map(i => ({
        ID: i.ID,
        InvoiceID: i.InvoiceID,
        Customer: i.InvoiceHeader && i.InvoiceHeader.DeliveryReceipt && i.InvoiceHeader.DeliveryReceipt.Customer ? i.InvoiceHeader.DeliveryReceipt.Customer.Name : "",
        AmountToCollect: i.AmountToCollect,
        AmountCollected: i.AmountCollected,
        Balance: i.Balance,
        Status: i.Status
      })) : []
    }));

    return this._collectionPlans;
  }

  async collectionPlanDetails(id: number) {
    return this.http.get('/collection-plans/get-collection-plan-details?planHeaderId=' + id);
  }

  async updateCollectionStatus(id: number, status: number) {
    return this.http.get('/collection-plans/update-collection-plan-detail-status?planDetailId=' + id + '&status=' + status);
  }

  async getCollectionListByCustomer(customerId: number) {
    const baseApi = await this.storage.get("API_URL");
    const data = await this.http.get(baseApi + '/api/collection-plan/get-per-customer/' + customerId, true);

    return data.map(x => ({
      ID: x.ID,
      Salesman: x.Salesman.Employee.Fullname,
      Collectible: x.TotalCollectible,
      Collected: x.TotalCollected,
      Balance: x.TotalBalance,
      Details: x.Details.map(i => ({
        InvoiceID: i.InvoiceHeader.ID,        
        InvoiceDate: i.InvoiceHeader.Date,
        PaymentTerms: i.InvoiceHeader.PaymentTerms,
        OverdueDays: 0,
        Tax: i.InvoiceHeader.TotalTax,
        Amount: i.InvoiceHeader.NetTotal,
        AmountPaid: i.InvoiceHeader.NetTotal,
        Status: i.InvoiceHeader.Status,
        RecordStatus: i.InvoiceHeader.RecordStatus
      }))
    }));
  }

  async getCustomerInvoices(customerId: number) {
    const baseApi = await this.storage.get("API_URL");
    const invoices = await this.http.get(baseApi + '/api/invoice/get-customer-invoices?customerId=' + customerId, true);

    return invoices.map(x => ({
      InvoiceID: x.ID,
      InvoiceDate: x.Date,
      PaymentTerms: x.PaymentTerms,
      OverdueDays: 0,
      Tax: x.TotalTax,
      Amount: x.NetTotal,
      AmountPaid: x.NetTotal
    }));
  } 

  formatResponse(x) {
    return (
      {
        ID: x.ID,
        Date: x.Date,
        CollectionPlanId: x.CollectionPlanId,
        ReferenceNo: x.ReferenceNo,
        CustomerID: x.CustomerID,
        CustomerName: x.Customer.Name,        
        TotalAmountDue: x.TotalAmountDue,
        TotalAmountPayable: x.TotalAmountPayable,
        TotalAmountPaid: x.TotalAmountPaid,
        CashAmount: x.CashAmount,
        PaymentType: x.PaymentType,
        Status: x.Status,
        CheckPayments: x.CheckPayments.map(i => ({
          ID: i.ID,
          CollectionID: i.CollectionID,
          BankCode: i.BankCode,
          DueDate: i.DueDate,
          CheckNo: i.CheckNo,
          Amount: i.Amount,
          IsPostDated: i.IsPostDated,
          Remarks: i.Remarks,
          Status: i.Status,
          RecordStatus: i.RecordStatus
        })),
        BankTransferPayments: x.BankTransferPayments.map(i => ({
          ID: i.ID,
          CollectionID: i.CollectionID,
          BankCode: i.BankCode,
          TransferDate: i.TransferDate,
          ReferenceNo: i.ReferenceNo,
          Amount: i.Amount,
          Remarks: i.Remarks,
          Status: i.Status,
          RecordStatus: i.RecordStatus
        })),
        Details: x.Details.map(i => (
          {
            ID: i.ID,
            InvoiceDate: i.Invoice.Date,
            InvoiceID: i.InvoiceID,
            CollectionID: i.CollectionID,
            PaymentTerms: i.PaymentTerms,
            OverdueDays: i.OverdueDays,
            Tax: i.Tax,
            Amount: i.Amount,
            AmountPaid: i.AmountPaid
          }
        ))
      }
    )
  }
}
