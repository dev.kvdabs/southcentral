import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { NavController } from '@ionic/angular';
import { ToastService } from './common/toast.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  baseApi = '';
  private headers: HttpHeaders;

  constructor(private storage: Storage, private httpClient: HttpClient, private nav: NavController, private toast: ToastService) { 
    
  }

  async setHeaders(force?: boolean) {
    if (force || !this.baseApi) {
      const api_url = await this.storage.get("API_URL");
      this.baseApi = api_url + "/api/salesman/";
    }

    if (force || !this.headers) {
      const user = await this.storage.get("USER_INFO");
      if (user) {
        this.headers = new HttpHeaders({
          'Authorization': 'Bearer ' + user.Token
        });
      }
    }
  }

  async get(api: string, base?: boolean): Promise<any> {
    await this.setHeaders();

    try {
      const url = !base ? this.baseApi + api : api;
      return await this.httpClient.get(url, {headers: this.headers}).toPromise()      
    } catch (error) {
      this.handleErrorResponse(error);
      throw error;
    }
  }

  async post(api: string, body: any) {
    await this.setHeaders();

    try {
      return await this.httpClient.post(this.baseApi + api, body, {headers: this.headers}).toPromise();
    } catch (error) {      
      this.handleErrorResponse(error);
      throw error;
    }    
  }
  
  async handleErrorResponse(error: HttpErrorResponse) {
    console.log('errror::' + error.message)
    if (error.status == 404) {
      this.toast.error(error.message, 3000, true);
    } else if (error.status == 402) {
      this.toast.error("Session expired", 2000, true)
    } else if (error.status == 401) {
      this.toast.error("Session expired", 2000, true)
      this.storage.remove("USER_INFO");
      this.nav.navigateRoot("login");
    }
  }
}
