import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Order } from '../models/order.model';
import { Subject } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class OrderService {
  private api = '';
  private _orders: any;
  $orders = new Subject<any[]>();

  constructor(private http: HttpService, private auth: AuthService) { 
    this.api = "/orders";

    this.auth.$loggedOut.subscribe(() => this._orders = []);
  }

  async orders(cache?: boolean) {
    if (cache) return this._orders; // for searching

    if (this._orders && this._orders.length !== 0) this.$orders.next(this._orders);

    let orders: any[] = await this.http.get(this.api);
    
    this._orders = orders.map(this.formatResponse);
    this.$orders.next(this._orders);
  }

  order(id: number) {
    return this.http.get(this.api + '?id=' + id);
  }

  async add(order: Order) {
    try {
      const res: any = await this.http.post(this.api + '/add', order);

      if (res.Message == "Success") {
        this._orders.push(this.formatResponse(res.Model));
        this.$orders.next(this._orders);

        return {
          success: true
        }
      }

      return {
        success: false,
        message: res.Message
      }
    } catch (error) {

      throw error
    }
  }

  async update(order: Order) {
    try {
      const res: any = await this.http.post(this.api + '/update', order);

      if (res.Message == "Success") {

        const index = this._orders.findIndex(x => x.ID == order.ID);
        this._orders.splice(index, 1, this.formatResponse(res.Model));
        this.$orders.next(this._orders);

        return {
          success: true
        }
      }

      return {
        success: false,
        message: res.Message
      }
    } catch (error) {

      throw error;
    }
  }  

  formatResponse(x) {
    return (
      {
        ID: x.ID,
        OrderDate:x.OrderDate,
        CustomerID: x.CustomerID,
        CustomerName: x.Customer.Name,
        OrderStatus: x.OrderStatus,
        OrderType: x.OrderType,
        ModeOfPayment: x.ModeOfPayment,
        GrossTotal: x.GrossTotal,
        TotalTax: x.TotalTax,
        TotalDiscount: x.TotalDiscount,
        NetTotal: x.NetTotal,
        Remarks: x.Remarks,
        Commission: x.Commission,
        Details: x.Details.map(i => (
          {
            ID: i.ID,
            SalesOrderID: i.SalesOrderID,
            ItemID: i.ItemID,
            ItemName: i.OrderItem.Name,
            Qty: i.Qty,
            Price: i.Price,
            Amount: i.Amount,
            RequestedPrice: i.RequestedPrice
          }
        ))
      }
    )
  }
}
