import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { HttpService } from './http.service';
import { BehaviorSubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private api = '';
  $currectUserDisplayName = new BehaviorSubject<string>('');
  $loggedOut = new Subject<any>();
  
  constructor(private storage: Storage, private http: HttpService) { 
    this.api = "auth"
  }

  authenticate(credential: any) {
    return this.http.post(this.api, credential);
  }

  currentUserDisplayName() {
    this.storage.get("USER_INFO").then(user => {
      if (user && user.SalesPerson) this.$currectUserDisplayName.next(user.SalesPerson.Employee.Fullname);
    });
  }

  logout() {
    this.storage.remove("USER_INFO");
    this.$loggedOut.next();
  }
}
