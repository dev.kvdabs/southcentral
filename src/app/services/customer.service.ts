import { Injectable } from '@angular/core';
import { HttpService } from './http.service';
import { Storage } from '@ionic/storage';
import { Subject } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private api = '';
  private _customers: any;
  $customers = new Subject<any>();

  constructor(private http: HttpService, private auth: AuthService, private storage: Storage) { 
    this.api = "/customers";
    this.auth.$loggedOut.subscribe(() => this._customers = []);
  }

  async customers(cache?: boolean) {
    if (cache && this._customers && this._customers.length !== 0) return this._customers; // for searching
    
    if (this._customers && this._customers.length !== 0) this.$customers.next(this._customers);

    return await this.getCustomers();
  }

  async getCustomers() {
    const customers = await this.http.get(this.api);

    this._customers = customers.map(this.formatResponse);

    this.$customers.next(this._customers);

    return this._customers;
  }

  async customer(id: number) {
    const _customer = await this.http.get(this.api + '?id=' + id);
    const customer = this.formatResponse(_customer);

    const index = this._customers.findIndex(x => x.ID == customer.ID);
    this._customers.splice(index, 1, );
    this.$customers.next(this._customers);

    return customer;
  }

  async add(customer) {
    try {
      const res: any = await this.http.post(this.api + "/add", customer);

      if (res.Message == "Success") {
        this._customers.push(this.formatResponse(res.Model));
        this.$customers.next(this._customers);

        return {
          success: true
        }
      }

      return {
        success: false,
        message: res.Message
      }

    } catch (error) {

      const pendingCustomers: any[] = (await this.storage.get("PENDING_CUSTOMERS")) || [];
      pendingCustomers.push(customer);
      
      this.storage.set("PENDING_CUSTOMERS", pendingCustomers)

      return {
        success: false
      }
    }
  }

  async update(customer) {
    try {
      
      const res: any = await this.http.post(this.api + "/update", customer);
      
      if (res.Message == "Success") {
        const index = this._customers.findIndex(x => x.ID == customer.ID);
        this._customers.splice(index, 1, this.formatResponse(res.Model));
        this.$customers.next(this._customers);

        return {
          success: true
        }
      }

    } catch (error) {

      const pendingCustomers: any[] = (await this.storage.get("PENDING_CUSTOMERS")) || [];
      const index = pendingCustomers.findIndex(x => x.ID == customer.ID)
      if (index !== -1) pendingCustomers.splice(index, 1, customer);

      this.storage.set("PENDING_CUSTOMERS", pendingCustomers)

      return {
        success: false
      }
    }
  }

  formatResponse(x) {
    return (
      {
        ID: x.ID,
        Name: x.Name,
        TIN: x.TIN,
        Email: x.Email,
        MobileNo: x.MobileNo,
        OfficePhoneNo: x.OfficePhoneNo,
        BusinessPhoneNo: x.BusinessPhoneNo,
        ContactPerson: x.ContactPerson,
        ContactNo: x.ContactNo,
        ContactEmail: x.ContactEmail,
        CityMunicipalityID: x.CityMunicipalityID,
        CityMunicipality: x.CityMunicipality,
        AreaID: x.AreaID,
        AreaName: x.Area.Name,
        Address: x.Address,
        ZipCode: x.ZipCode,
        BillToAddress: x.BillToAddress,
        ShipToAddress: x.ShipToAddress,
        Status: x.Status,
        RegionID: x.RegionID,
        Region: x.Region,
        ProvinceID: x.ProvinceID,
        Province: x.Province,
        TerritoryID: x.TerritoryID,
        TerritoryName: x.Territory.TerritoryName,
        Barangay: x.Barangay,
        CreditLines: x.CreditLines ? x.CreditLines.filter(cl => cl.ItemType).map(i => ({
          Name: i.ItemType.Name,
          CreditLimit: i.CreditLimit,
          Balance: i.Balance
        })) : []
      }
    )
  }

  async municipalities() {
    return await this.http.get('assets/data/municipalities.json', true);    
  }

  async territories() {
    const baseApi = await this.storage.get("API_URL");
    const _territories = await this.http.get(baseApi + 'api/territory/get-list', true);

    return _territories.map(x => ({
      ID: x.ID,  
      TerritoryName: x.TerritoryName,  
      AreaID: x.Area.ID,      
      AreaName: x.Area.Name
    }));
  }

  async getTerritory(cityMunCode: number) {
    const baseApi = await this.storage.get("API_URL");
    return await this.http.get(baseApi + 'api/territory/get-territory-by-citymunicipality?cityMunicipalityId=' + cityMunCode, true);
  }
}
