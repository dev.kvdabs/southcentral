import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class ItemService {
  private api = '';

  private _customerItems = [];

  constructor(private http: HttpService) { 
    this.api = "/item"
  }

  items() {
    return this.http.get(this.api);
  }

  async customerItems(customerId: string) {
    const ci = this._customerItems.find(x => x.id == customerId);
    if (ci) return ci.items;

    let items = await this.http.get(this.api + '/get-items?customerId=' + customerId);
    items = items.map(this.formatResponse)
    
    items = this.sort(items);
    
    this._customerItems.push({
      id: customerId,
      items: items
    });

    return items;
  }

  formatResponse(x) {
    return (
      {
        ID: x.Item.ID,
        Name: x.Item.Name,
        CustomerMinimumQty: x.CustomerMinimumQty,
        CustomerPrice: x.CustomerPrice,
        DefaultRetailPrice: x.DefaultRetailPrice,
        LastPrice: x.LastPrice,
        RemainingQty: x.RemainingQty
      }
    )
  }

  sort(items) {
    items = items.sort(function (a, b) {
      if (a.Name > b.Name) {
          return 1;
      }
      if (b.Name > a.Name) {
          return -1;
      }
      return 0;
    });

    return items;
  }

  getPrice(item) {
    if (item.CustomerMinimumQty == 0) return item.DefaultRetailPrice;
    else if (item.Qty >= item.CustomerMinimumQty) return item.CustomerPrice;
    
    return item.DefaultRetailPrice;
  }
}
