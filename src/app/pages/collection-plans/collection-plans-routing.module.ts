import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CollectionPlansPage } from './collection-plans.page';

const routes: Routes = [
  {
    path: '',
    component: CollectionPlansPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CollectionPlansPageRoutingModule {}
