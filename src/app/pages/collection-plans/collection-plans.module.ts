import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CollectionPlansPageRoutingModule } from './collection-plans-routing.module';

import { CollectionPlansPage } from './collection-plans.page';
import { CollectionPlanComponent } from './collection-plan/collection-plan.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CollectionPlansPageRoutingModule
  ],
  declarations: [CollectionPlansPage, CollectionPlanComponent],
  entryComponents: [CollectionPlanComponent]
})
export class CollectionPlansPageModule {}
