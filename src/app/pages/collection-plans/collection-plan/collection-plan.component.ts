import { Component, OnInit } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { CollectionPlan } from 'src/app/models/collection.model';

@Component({
  selector: 'app-collection-plan',
  templateUrl: './collection-plan.component.html',
  styleUrls: ['./collection-plan.component.scss'],
})
export class CollectionPlanComponent implements OnInit {
  title = '';
  collectionPlan: CollectionPlan;

  constructor(private params: NavParams, private _modal: ModalController) {
    this.collectionPlan = this.params.get('data');
    this.title = "Collection Plan #: " + this.collectionPlan.ID
  }

  ngOnInit() {}

  back() {
    this._modal.dismiss();
  }

}
