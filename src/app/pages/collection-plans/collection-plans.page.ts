import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CollectionService } from '../../services/collection.service';
import { ToastService } from 'src/app/services/common/toast.service';
import { HelperService } from 'src/app/services/common/helper.service';
import { CollectionPlan } from 'src/app/models/collection.model';
import { CollectionPlanComponent } from './collection-plan/collection-plan.component';

@Component({
  selector: 'app-collection-plans',
  templateUrl: './collection-plans.page.html',
  styleUrls: ['./collection-plans.page.scss'],
})
export class CollectionPlansPage implements OnInit {

  collectionPlans: any;

  constructor(private modal: ModalController, 
              private helper: HelperService,
              private toast: ToastService,
              private collectionService: CollectionService) { }

  async ngOnInit() {
    try {
      this.collectionPlans = await this.collectionService.collectionplans()
    } catch (error) {
      this.collectionPlans = [];
    }
  }

  async doRefresh(event) {
    try {
      this.collectionPlans = await this.collectionService.collectionplans();      
    } catch (error) {
      this.toast.error(null, null, true);
    } finally {
      event.target.complete()
    }
  }

  async search(event) {
    if (event) {  
      const arr = await this.collectionService.collectionplans(true);
      this.collectionPlans = this.helper.filter(arr, ["ID", "ReleaseDate"], event.target.value);
    }      
  }

  async detail(collectionPlan?: CollectionPlan) {
    const modal = await this.modal.create({
      component: CollectionPlanComponent,
      componentProps: {
        data: collectionPlan
      }
    });

    modal.present();
  }
}
