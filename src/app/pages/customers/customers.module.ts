import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomersPageRoutingModule } from './customers-routing.module';

import { CustomersPage } from './customers.page';
import { CustomerComponent } from './customer/customer.component';
import { CustomerStatusPipeModule } from 'src/app/pipes/customer-status/customer-status-pipe.module';
import { SelectCityModule } from 'src/app/components/select-city/select-city.module';
import { SelectCityComponent } from 'src/app/components/select-city/select-city.component';
import { SelectTerritoryModule } from 'src/app/components/select-territory/select-territory.module';
import { SelectTerritoryComponent } from 'src/app/components/select-territory/select-territory.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    CustomersPageRoutingModule,
    CustomerStatusPipeModule,
    SelectCityModule,
    SelectTerritoryModule
  ],
  declarations: [CustomersPage, CustomerComponent],
  entryComponents: [CustomerComponent, SelectCityComponent, SelectTerritoryComponent]
})
export class CustomersPageModule {}
