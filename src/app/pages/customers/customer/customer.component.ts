import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams, NavController } from '@ionic/angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Customer } from 'src/app/models/customer.model';
import { CustomerService } from 'src/app/services/customer.service';
import { ToastService } from 'src/app/services/common/toast.service';
import { LoadingService } from 'src/app/services/common/loading.service';
import { CUSTOMER_STATUS } from 'src/app/constants/enums';
import { HelperService } from 'src/app/services/common/helper.service';
import { SelectCityComponent } from 'src/app/components/select-city/select-city.component';
import { SelectTerritoryComponent } from 'src/app/components/select-territory/select-territory.component';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
})
export class CustomerComponent implements OnInit {
  title: string = 'New Customer';
  
  form: FormGroup;
  customer: Customer;

  constructor(private _modal: ModalController, 
              private params: NavParams, 
              private customerService: CustomerService,
              private toast: ToastService,
              private loading: LoadingService,
              private helper: HelperService,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.customer = this.params.get('customer') || {};

    if (this.customer.ID) this.title = 'Customer #: ' + this.customer.ID;    
    this.initForm(this.customer);

    //this.getCustomer();
  }

  getCustomer() {
    if (this.customer.ID) this.customerService.customer(this.customer.ID).then(customer => this.initForm(customer));
  }

  initForm(object: any) {
    this.form = this.fb.group({ 
      ID: [0],
      Name: ['', Validators.required],
      TIN: [''],
      Email: [''],
      MobileNo: [''],
      OfficePhoneNo: [''],
      BusinessPhoneNo: [''],
      ContactPerson: ['', Validators.required],
      ContactNo: ['', Validators.required],
      ContactEmail: [''],      
      Address: ['', Validators.required],
      ZipCode: ['', Validators.required],
      BillToAddress: [''],
      ShipToAddress: [''],
      CityMunicipalityID: [null, Validators.required],
      RegionID: [null, Validators.required],
      ProvinceID: [null, Validators.required],
      TerritoryID: [null, Validators.required],
      TerritoryName: [''],
      AreaID: [null, Validators.required],      
      AreaName: [''],
      Region: ['', Validators.required],
      Province: ['', Validators.required],
      CityMunicipality: ['', Validators.required],
      Barangay: [''],
      Status: [CUSTOMER_STATUS.Prospect],
    });

    for (var key in object) {
      this.helper.setFormValue(this.form, key, object[key]);
    }
  }

  async selectCity() {
    const modal = await this._modal.create({
      component: SelectCityComponent
    });

    modal.onDidDismiss().then(res => {
      if (res && res.data) {
        this.helper.setFormValue(this.form, "CityMunicipalityID", res.data.id);
        this.helper.setFormValue(this.form, "CityMunicipality", res.data.name);
        this.helper.setFormValue(this.form, "RegionID", res.data.region.id);
        this.helper.setFormValue(this.form, "Region", res.data.region.name);
        this.helper.setFormValue(this.form, "ProvinceID", res.data.province.id);
        this.helper.setFormValue(this.form, "Province", res.data.province.name);

        this.customerService.getTerritory(res.data.cityMunCode).then(territory => {
          this.helper.setFormValue(this.form, "TerritoryID", territory.ID);
          this.helper.setFormValue(this.form, "TerritoryName", territory.TerritoryName);
          this.helper.setFormValue(this.form, "AreaID", territory.Area.ID);
          this.helper.setFormValue(this.form, "AreaName", territory.Area.Name);
        })
      }
    })

    modal.present();
  }

  async selectTerritory() {
    const modal = await this._modal.create({
      component: SelectTerritoryComponent
    });

    modal.onDidDismiss().then(res => {
      if (res && res.data) {
        this.helper.setFormValue(this.form, "TerritoryID", res.data.id);
        this.helper.setFormValue(this.form, "TerritoryName", res.data.name);
        this.helper.setFormValue(this.form, "AreaID", res.data.area.id);
        this.helper.setFormValue(this.form, "AreaName", res.data.area.name);
      }
    })

    modal.present();
  }

  back() {
    this._modal.dismiss();
  }

  async save() {
    const loader = await this.loading.create('Saving...');

    try {
      
      if (!this.form.valid) return;

      loader.present();

      this.customer = this.form.value;

      if (this.customer.ID) {
        const data = await this.customerService.update(this.customer);        
      }
      else await this.customerService.add(this.customer);

      this.back();

    } catch (error) {
      
      const toast = await this.toast.error(null, 2000);
      toast.present();
      
    } finally {
      if (loader) loader.dismiss();
    }
  }

  getFormValue(prop: string) {
    return this.helper.getFormValue(this.form, prop);
  }
}
