import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CustomerComponent } from './customer/customer.component';
import { CustomerService } from '../../services/customer.service';

import { Subscription } from 'rxjs';
import { ToastService } from 'src/app/services/common/toast.service';
import { HelperService } from 'src/app/services/common/helper.service';
import { CUSTOMER_STATUS } from 'src/app/constants/enums';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.page.html',
  styleUrls: ['./customers.page.scss'],
})
export class CustomersPage implements OnInit {
  customers: any;
  $customersSub: Subscription;

  constructor(private modal: ModalController, private toast: ToastService, private helper: HelperService, private customerService: CustomerService) { }

  async ngOnInit() {
    try {      
      this.$customersSub = this.customerService.$customers.subscribe(customers => {        
        this.customers = customers;
      });

      this.customerService.customers();
      this.customerService.municipalities();
    } catch (error) {
      this.customers = [];
    }
  }

  ngOnDestroy() {
    if (this.$customersSub) this.$customersSub.unsubscribe();
  }

  async doRefresh(event) {
    try {
      
      await this.customerService.customers();
      this.toast.success("Customer list updated.", null, true);

    } catch (error) {
      this.toast.error(null, null, true);
    } finally {
      event.target.complete();
    }
  }

  async search(event) {
    if (event) {  
      const arr = await this.customerService.customers(true);
      this.customers = this.helper.filter(arr, ["Name"], event.target.value);
    }
  }

  add() {
    this.detail();
  }

  async detail(customer?: any) {
    const modal = await this.modal.create({
      component: CustomerComponent,
      componentProps: {
        customer
      }
    });

    modal.present();
  }

  statusColor(status: number) {
    let color = "c-medium";

    if (status == CUSTOMER_STATUS.Verified) color = "c-primary";

    return color;
  }
}
