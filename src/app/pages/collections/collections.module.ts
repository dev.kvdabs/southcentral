import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CollectionsPageRoutingModule } from './collections-routing.module';

import { CollectionsPage } from './collections.page';
import { CollectionComponent } from './collection/collection.component';
import { PaymentModule } from '../../components/payment/payment.module';
import { PaymentComponent } from '../../components/payment/payment.component';
import { SelectCustomerModule } from '../../components/select-customer/select-customer.module';
import { SelectCustomerComponent } from '../../components/select-customer/select-customer.component';
import { SelectInvoiceModule } from '../../components/select-invoice/select-invoice.module';
import { SelectInvoiceComponent } from '../../components/select-invoice/select-invoice.component';
import { CollectionStatusPipeModule } from 'src/app/pipes/collection-status/collection-status-pipe.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    CollectionsPageRoutingModule,
    PaymentModule,
    SelectCustomerModule,
    SelectInvoiceModule,
    CollectionStatusPipeModule
  ],
  declarations: [CollectionsPage, CollectionComponent],
  entryComponents: [CollectionComponent, PaymentComponent, SelectCustomerComponent, SelectInvoiceComponent]
})
export class CollectionsPageModule {}
