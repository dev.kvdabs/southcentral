import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CollectionComponent } from './collection/collection.component';
import { CollectionService } from '../../services/collection.service';
import { ToastService } from 'src/app/services/common/toast.service';
import { Subscription } from 'rxjs';
import { Collection } from 'src/app/models/collection.model';
import { HelperService } from 'src/app/services/common/helper.service';
import { COLLECTION_STATUS } from 'src/app/constants/enums';

@Component({
  selector: 'app-collections',
  templateUrl: './collections.page.html',
  styleUrls: ['./collections.page.scss'],
})
export class CollectionsPage implements OnInit {

  collections: any;
  $collectionsSub: Subscription;

  constructor(private modal: ModalController, 
              private toast: ToastService,
              private helper: HelperService,
              private collectionService: CollectionService) { }

  async ngOnInit() {
    try {
      this.$collectionsSub = this.collectionService.$collections.subscribe(collections => this.collections = collections);
      this.collectionService.collections();      
    } catch (error) {
      this.collections = [];
    }
  }

  ngOnDestroy() {
    if (this.$collectionsSub) this.$collectionsSub.unsubscribe();
  }

  async doRefresh(event) {
    try {
      
      await this.collectionService.collections();
      
    } catch (error) {
      this.toast.error(null, null, true);
    } finally {
      event.target.complete();
    }
  }

  async search(event) {
    if (event) {  
      const arr = await this.collectionService.collections(true);
      this.collections = this.helper.filter(arr, ["ID", "CustomerName"], event.target.value);
    }      
  }

  add() {
    this.detail();
  }

  async detail(collection?: Collection) {
    const modal = await this.modal.create({
      component: CollectionComponent,
      componentProps: {
        collection
      }
    });

    modal.present();
  }

  statusColor(status: number) {
    let color = "c-medium";

    if (status == COLLECTION_STATUS.Verified) color = "c-primary";

    return color;
  }
}
