import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { PaymentComponent } from 'src/app/components/payment/payment.component';
import { PAYMENT_METHOD } from 'src/app/constants/enums';
import { FormatUtilService } from 'src/app/services/common/format-util.service';
import { SelectCustomerComponent } from 'src/app/components/select-customer/select-customer.component';
import { SelectInvoiceComponent } from 'src/app/components/select-invoice/select-invoice.component';
import { Collection, CollectionDetail, ChequePayment, BankTransferPayment } from 'src/app/models/collection.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from 'src/app/services/common/toast.service';
import { AlertService } from 'src/app/services/common/alert.service';
import { HelperService } from 'src/app/services/common/helper.service';
import { CollectionService } from 'src/app/services/collection.service';
import { LoadingService } from 'src/app/services/common/loading.service';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss'],
})
export class CollectionComponent implements OnInit {
  title = "New Collection";
  detail = "invoices";
  showDetail: boolean;

  paymentMethods = [];
  
  form: FormGroup;

  cashAmount: number;

  collection: Collection;
  invoices: CollectionDetail[] = [];
  chequePayments: ChequePayment[] = [];
  bankTransferPayments: BankTransferPayment[] = [];

  constructor(private _modal: ModalController, 
              private formatUtil: FormatUtilService, 
              private params: NavParams, 
              private fb: FormBuilder,
              private toast: ToastService,
              private helper: HelperService,
              private collectionService: CollectionService,
              private loading: LoadingService,
              private alert: AlertService) { }

  ngOnInit() {
    this.paymentMethods = this.helper.constToArray(PAYMENT_METHOD);
    this.collection = this.params.get('collection') || {};

    if (this.collection.ID) this.title = 'Collection #: ' + this.collection.ID;
    this.showDetail = this.collection.CustomerID !== null;
    this.initForm(this.collection);
  }

  initForm(object: any) {    
    this.form = this.fb.group({
      ID: [0],
      CustomerID: ['', Validators.required],
      CustomerName: [''],
      ReferenceNo: [''],
      Date: [new Date().toISOString(), Validators.required],
      CollectionPlanId: [null],
      TotalAmountDue: [0, Validators.required],
      TotalAmountPayable: [0, Validators.required],
      CashAmount: [0, Validators.required],
      TotalAmountPaid: [0, Validators.required],
      PaymentType: [PAYMENT_METHOD.CHEQUE, Validators.required],
      Status: [0]
    });

    for (var key in object) {      
      this.helper.setFormValue(this.form, key, object[key]);
    }

    if (object.Details) this.invoices = object.Details;
    if (object.CheckPayments) this.chequePayments = object.CheckPayments;
    if (object.BankTransferPayments) this.bankTransferPayments = object.BankTransferPayments;

    this.collection.CashAmount = this.helper.getFormValue(this.form, "CashAmount");
    this.cashAmount = this.collection.CashAmount;    
  }

  back() {
    this._modal.dismiss();
  }

  async selectCustomer() {
    const modal = await this._modal.create({
      component: SelectCustomerComponent
    });

    modal.onDidDismiss().then(res => {
      
      if (res && res.data) {
        this.helper.setFormValue(this.form, 'CustomerID', res.data.id);
        this.helper.setFormValue(this.form, 'CustomerName', res.data.name);

        this.invoices = [];      
        this.computeInvoiceTotals();        
        this.showDetail = true;
      }
      
    })

    modal.present();
  }
  
  add() {
    if (this.detail == "payments") this.payment();
    else if (this.detail == "invoices") this.invoice();
  }

  async payment(data?: any, index?: number) {
    const paymentType = this.form.controls["PaymentType"].value;

    const modal = await this._modal.create({
      component: PaymentComponent,
      componentProps: {
        paymentMethod: {
          id: paymentType,
          name: this.paymentMethods[paymentType].name
        },
        data: data
      }
    });
    
    modal.onDidDismiss().then(res => {
      if (res && res.data) {
        if (this.isCheque()) {
          
          if (index == undefined) this.chequePayments.push(res.data);
          else this.chequePayments[index] = res.data;

        } else if (this.isBankTransfer()) {

          if (index == undefined) this.bankTransferPayments.push(res.data);
          else this.bankTransferPayments[index] = res.data;  

        }

        this.computeAmountPaidTotals();
      }
    })

    modal.present();
  }

  async invoice() {
    
    const modal = await this._modal.create({
      component: SelectInvoiceComponent,
      componentProps: {
        customerId: this.getFormValue("CustomerID"),
        ids: this.invoices.map(x => x.InvoiceID)
      }
    });

    modal.onDidDismiss().then(res => {
      if (res && res.data) {
        this.invoices = [];

        if (res.data.ID) {
          // Collection Plan
          this.helper.setFormValue(this.form, 'CollectionPlanId', res.data.ID);
          this.invoices.push(...res.data.Details);
        } else {
          // Invoices
          this.helper.setFormValue(this.form, 'CollectionPlanId', null);
          this.invoices = res.data;          
        }

        this.computeInvoiceTotals();
        this.computeAmountPaidTotals();
      }
    })

    modal.present();
  }

  async invoicePayment(invoice: CollectionDetail) {

    const button = {
      text: 'Continue',
      handler: async (data) => {
        if (data && data.amount) {
          invoice.AmountPaid = parseFloat(data.amount);

          this.helper.setFormValue(this.form, 'TotalAmountPaid', this.helper.sum(this.invoices, 'AmountPaid'));          
        }
      }
    } 

    const alert = await this.alert.create('Amount to Pay', 'Invoice #: ' + invoice.InvoiceID,      
      [
        {
          name: 'amount',
          type: 'number',
          placeholder: "0.00"
        }
      ],
      button
    )

    alert.present();
  }

  computeInvoiceTotals() {
    this.helper.setFormValue(this.form, 'TotalAmountDue', this.helper.sum(this.invoices, 'Amount'));      
    this.helper.setFormValue(this.form, 'TotalAmountPayable', this.helper.sum(this.invoices, 'Amount'));
  }

  computeAmountPaidTotals() {
    //if (this.isCheque()) this.helper.setFormValue(this.form, 'TotalAmountPaid', this.helper.sum(this.chequePayments, 'Amount'));
    //else if (this.isBankTransfer()) this.helper.setFormValue(this.form, 'TotalAmountPaid', this.helper.sum(this.bankTransferPayments, 'Amount'));
    this.helper.setFormValue(this.form, 'TotalAmountPaid', this.helper.sum(this.invoices, 'AmountPaid'));
  }

  get isCash() {
    return this.helper.getFormValue(this.form, 'PaymentType') === 0;
  }

  isBankTransfer() {
    return this.helper.getFormValue(this.form, 'PaymentType') === 1;
  }

  isCheque() {
    return this.helper.getFormValue(this.form, 'PaymentType') === 2
  }

  getFormValue(prop: string) {
    return this.helper.getFormValue(this.form, prop);
  }

  removeInvoice(index) {
    this.invoices.splice(index, 1);
    this.computeInvoiceTotals();
  }

  removePayment(index) {
    if (this.isCheque()) this.chequePayments.splice(index, 1);
    else if (this.isBankTransfer()) this.bankTransferPayments.splice(index, 1);

    this.computeAmountPaidTotals();
  }

  async save() {
    const loader = await this.loading.create('Saving...');

    try {
      // if (!this.form.valid) return;

      loader.present();

      this.collection = this.form.value;

      this.collection.Date = this.formatUtil.formatDate(this.collection.Date);
      this.collection.Details = this.invoices;
      
      if (this.collection.PaymentType == PAYMENT_METHOD.CHEQUE) {
        // Cheque Payments
        this.chequePayments.forEach(x => x.DueDate = this.formatUtil.formatDate(x.DueDate));
        this.collection.CheckPayments = this.chequePayments;

        this.collection.BankTransferPayments = [];
        this.collection.CashAmount = 0;

      } else if (this.collection.PaymentType == PAYMENT_METHOD.BANK_TRANSFER) {
        // Bank Transfer Payments
        this.bankTransferPayments.forEach(x => x.TransferDate = this.formatUtil.formatDate(x.TransferDate))
        this.collection.BankTransferPayments = this.bankTransferPayments;

        this.collection.CheckPayments = [];
        this.collection.CashAmount = 0;
      }

      if (this.collection.PaymentType == PAYMENT_METHOD.CASH) {
        if (!this.cashAmount || this.cashAmount <= 0) {
          const toast = await this.toast.error("Please enter cash amount", 500);
          toast.present();
          return;
        }

        this.collection.CashAmount = this.cashAmount;
        this.collection.CheckPayments = [];
        this.collection.BankTransferPayments = [];
      }
      
      const res: any = await (this.collection.ID ? this.collectionService.update(this.collection) : this.collectionService.add(this.collection))

      if (res.success) this.back();
      else {
        this.toast.error(res.message, 2000, true);        
      }

    } catch (error) {
      console.log(error)
      const toast = await this.toast.error(null, 2000);
      toast.present();
    } finally {
      if (loader) loader.dismiss();
    }
  }

  
}
