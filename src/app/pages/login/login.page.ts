import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, MenuController, ToastController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { AuthService } from '../../services/auth.service';
import { HttpService } from '../../services/http.service';
import { ToastService } from 'src/app/services/common/toast.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  apiUrl: string;
  showLogin = true;

  authenticating: boolean;

  username = '';
  password = '';

  constructor(private alertCtrl: AlertController, 
    private storage: Storage, 
    private nav: NavController, 
    private menu: MenuController, 
    private toast: ToastService,
    private http: HttpService,
    private authService: AuthService) { }

  async ngOnInit() {
    this.menu.swipeGesture(false);
    this.apiUrl = await this.storage.get("API_URL");

    if (!this.apiUrl) {
      //this.openConfig();
      this.apiUrl = "http://112.198.195.29:1500/";
      this.storage.set("API_URL", this.apiUrl);
    }
  }

  async openConfig() {
    const alert = await this.alertCtrl.create({
      header: 'Enter API URL',      
      mode: "ios",
      backdropDismiss: false,
      inputs: [
        {
          name: 'api',
          type: 'text',
          value: this.apiUrl,
          placeholder: "http://"
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Set',
          handler: async (data) => {

            if (data.api) this.setConfig(data.api);

          }
        }        
      ]
    });

    alert.present();
  }

  async setConfig(url: string) {
    await this.storage.set("API_URL", url);
    this.apiUrl = url;
    this.http.setHeaders(true);
  }

  async login() {

    this.authenticating = true;

    this.authService.authenticate({
      username: this.username,
      password: this.password
    }).then(async (response: any) => {
      if (response.Message == "Success") {
        await this.storage.set("USER_INFO", response);
        await this.http.setHeaders(true);

        this.authService.currentUserDisplayName();

        this.authenticating = false;
        this.menu.swipeGesture(true);
        return this.nav.navigateRoot("orders");
      }

      this.handleFailedLogin(response.Message);   
           
    }, async(error) => {
      this.handleFailedLogin(JSON.stringify(error));      
    });
  }

  async handleFailedLogin(message?: string) {
    this.authenticating = false;

    this.toast.error(message || 'Invalid login credentials', 2000, true);
    
  }
}
