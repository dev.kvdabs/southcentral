import { Component } from '@angular/core';
import { Storage } from '@ionic/storage'
import { NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private storage: Storage, private nav: NavController, private authService: AuthService) {
    this.storage.get("USER_INFO").then(user => {
      setTimeout(() => {
        if (user) {
          this.authService.$currectUserDisplayName.next(user.SalesPerson.Employee.Fullname);
          return this.nav.navigateRoot("orders");
        }
        return this.nav.navigateRoot("login");

      }, 1000);
    })
  }

}
