import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrdersPageRoutingModule } from './orders-routing.module';

import { OrdersPage } from './orders.page';
import { OrderComponent } from './order/order.component';
import { SelectCustomerModule } from '../../components/select-customer/select-customer.module';
import { SelectCustomerComponent } from '../../components/select-customer/select-customer.component';
import { SelectItemModule } from '../../components/select-item/select-item.module';
import { SelectItemComponent } from '../../components/select-item/select-item.component';
import { OrderStatusPipeModule } from 'src/app/pipes/order-status/order-status-pipe.module';

    
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    OrdersPageRoutingModule,
    SelectCustomerModule,
    SelectItemModule,
    OrderStatusPipeModule
  ],
  entryComponents: [OrderComponent, SelectCustomerComponent, SelectItemComponent],
  declarations: [OrdersPage, OrderComponent]
})
export class OrdersPageModule {}
