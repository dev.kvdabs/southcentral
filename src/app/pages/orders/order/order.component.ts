import { Component, OnInit } from '@angular/core';
import { ModalController, PopoverController, NavParams, AlertController } from '@ionic/angular';
import { SelectCustomerComponent } from 'src/app/components/select-customer/select-customer.component';
import { SelectItemComponent } from 'src/app/components/select-item/select-item.component';
import { PAYMENT_METHOD, ORDER_TYPE, ORDER_STATUS } from 'src/app/constants/enums';
import { FormatUtilService } from 'src/app/services/common/format-util.service';
import { Order, OrderItem } from 'src/app/models/order.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { OrderService } from 'src/app/services/order.service';
import { HelperService } from 'src/app/services/common/helper.service';
import { LoadingService } from 'src/app/services/common/loading.service';
import { ToastService } from 'src/app/services/common/toast.service';
import { ItemService } from 'src/app/services/item.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
})
export class OrderComponent implements OnInit {
  title = 'New Order';

  orderDate: any = new Date().toISOString();
  paymentMethods = [];
  orderTypes = [];

  order: Order;
  orderItems: OrderItem[] = [];
  form: FormGroup;

  showAddOrderItem: boolean;
  viewMode = 'details';

  constructor(private params: NavParams, 
              private _modal: ModalController, 
              private formatUtil: FormatUtilService, 
              private fb: FormBuilder,
              private orderService: OrderService,
              private helper: HelperService,
              private loading: LoadingService,
              private toast: ToastService,
              private itemService: ItemService,
              private alert: AlertController) { 
    this.order = this.params.get('order') || {};

    if (this.order.ID) this.title = 'Order #: ' + this.order.ID;
    if (this.order.CustomerID) this.showAddOrderItem = true;
    console.log(this.order)
    this.initForm(this.order);
  }

  ngOnInit() {

    this.paymentMethods = this.helper.constToArray(PAYMENT_METHOD);
    this.orderTypes = this.helper.constToArray(ORDER_TYPE);

  }

  initForm(object: any) {
    this.form = this.fb.group({ 
      ID: [0],
      OrderDate: [new Date().toISOString(), Validators.required],
      DeliveryDate: [new Date().toISOString(), Validators.required],
      CustomerID: [null, Validators.required],
      CustomerName: [''],
      ModeOfPayment: [PAYMENT_METHOD.CASH, Validators.required],
      OrderType: [ORDER_TYPE.DELIVERY, Validators.required],
      OrderStatus: [ORDER_STATUS.Pending],
      GrossTotal: [0],
      TotalTax: [0],
      TotalDiscount: [0],
      NetTotal: [0],
      Remarks: [''],
      Commission: []
    });

    for (var key in object) {
      this.helper.setFormValue(this.form, key, object[key]);
    }

    if (object.Details) this.orderItems = object.Details;
  }

  back() {
    this._modal.dismiss();
  }

  segmentChanged(event) {
    if (event) this.viewMode = event.detail.value;
  }

  async selectCustomer() {

    if (this.order.ID) return;

    const modal = await this._modal.create({
      component: SelectCustomerComponent
    });

    modal.onDidDismiss().then(res => {
      if (res && res.data) {
        if (this.helper.getFormValue(this.form, "CustomerID") != res.data.id) {

          this.helper.setFormValue(this.form, "CustomerID", res.data.id);
          this.helper.setFormValue(this.form, "CustomerName", res.data.name);
          
          this.showAddOrderItem = true;
          this.orderItems = [];
        }        
      }
    })

    modal.present();
  }

  async addOrderItem() {
    const customerID = this.helper.getFormValue(this.form, "CustomerID");

    const modal = await this._modal.create({
      component: SelectItemComponent,
      componentProps: {
        customerId: customerID,
        selected: this.orderItems.map(x => x.ItemID)
      }
    });

    modal.onDidDismiss().then(res => {
      if (res && res.data) {
        res.data['ID'] = 0;
        res.data['SalesOrderID'] = this.helper.getFormValue(this.form, "ID");

        const index = this.orderItems.findIndex(x => x.ItemID == res.data.ItemID);
        if (index > -1) {
          this.orderItems.splice(index, 1, res.data);
        }
        else this.orderItems.push(res.data);

        this.computeTotal();
      }  
    })

    modal.present();
  }

  minus(item: OrderItem) {
    if (item.Qty <= 1) return;
    item.Qty -= 1;

    this.computeItemTotal(item);
  }

  plus(item: OrderItem) {    
    item.Qty += 1;

    this.computeItemTotal(item);
  }

  computeItemTotal(item: OrderItem) {
    //item.Price = this.itemService.getPrice(item); // get customer pricing

    item.Amount = item.Qty * item.Price;

    this.computeTotal();
  }

  computeTotal() {
    this.helper.setFormValue(this.form, "GrossTotal", this.helper.sum(this.orderItems, "Amount"));
    this.helper.setFormValue(this.form, "NetTotal", this.helper.getFormValue(this.form, "GrossTotal"));
  }

  async priceRequest(item: any) {
    const alert = await this.alert.create({
      header: 'Price Request',    
      message: item.ItemName,
      mode: "ios",
      backdropDismiss: false,
      inputs: [
        {
          name: 'price',
          type: 'number',
          placeholder: "0.00"
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Continue',
          handler: async (data) => {
            if (data && data.price) item.RequestedPrice = parseFloat(data.price);
          }
        }        
      ]
    });

    alert.present();
  }

  remove(index) {
    this.orderItems.splice(index, 1);
    this.computeTotal();
  }

  async save() {
    const loader = await this.loading.create('Saving...');
    try {

      if (!this.form.valid) return;

      loader.present();

      this.order = this.form.value;
      
      this.order.OrderDate = this.formatUtil.formatDate(this.order.OrderDate);
      this.order.DeliveryDate = this.formatUtil.formatDate(this.order.DeliveryDate);    
      this.order.Details = this.orderItems;
      
      console.log(this.order);

      const res: any = await (this.order.ID ? this.orderService.update(this.order) : this.orderService.add(this.order))

      if (res.success) this.back();
      else {
        this.toast.error(res.message, 2000, true);        
      }

    } catch (error) {      
      this.toast.error(null, 2000, true);            
    } finally {
      if (loader) loader.dismiss();
    }
  }

  getFormValue(key: string) {
    return this.helper.getFormValue(this.form, key);
  }

  get ready_only () {
    return this.getFormValue("OrderStatus") == ORDER_STATUS.Completed || this.getFormValue("OrderStatus") == ORDER_STATUS.Cancelled;
  }
}