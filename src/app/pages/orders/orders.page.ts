import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { OrderComponent } from './order/order.component';
import { OrderService } from '../../services/order.service';
import { Order } from 'src/app/models/order.model';
import { ToastService } from 'src/app/services/common/toast.service';
import { Subscription } from 'rxjs';
import { HelperService } from 'src/app/services/common/helper.service';
import { ORDER_STATUS } from 'src/app/constants/enums';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.page.html',
  styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {
  orders: any;
  $ordersSub: Subscription;

  constructor(private modal: ModalController, 
              private orderService: OrderService, 
              private helper: HelperService,
              private toast: ToastService) { }

  async ngOnInit() {
    try {
      this.$ordersSub = this.orderService.$orders.subscribe(orders => this.orders = orders);
      this.orderService.orders();      
    } catch (error) {
      this.orders = [];
    }
  }

  ngOnDestroy() {
    if (this.$ordersSub) this.$ordersSub.unsubscribe();
  }

  async doRefresh(event) {
    try {
      
      await this.orderService.orders();
      
    } catch (error) {
      this.toast.error(null, null, true);
    } finally {
      event.target.complete();
    }
  }

  async search(event) {
    if (event) {  
      const arr = await this.orderService.orders(true);
      this.orders = this.helper.filter(arr, ["ID", "CustomerName"], event.target.value);
    }      
  }

  async add() {
    const modal = await this.modal.create({
      component: OrderComponent
    });

    modal.present();
  }

  async detail(order) {
    const modal = await this.modal.create({
      component: OrderComponent,
      componentProps: {
        order
      }
    });

    modal.present();
  }

  statusColor(status: number) {
    let color = "c-medium";

    if (status == ORDER_STATUS.Completed) color = "c-primary";
    else if (status == ORDER_STATUS.Declined) color = "c-danger";

    return color;
  }
}
