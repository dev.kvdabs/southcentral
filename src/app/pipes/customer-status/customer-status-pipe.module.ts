import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomerStatusPipe } from './customer-status.pipe';

@NgModule({
  declarations: [CustomerStatusPipe],
  imports: [
    CommonModule
  ],
  exports: [CustomerStatusPipe]
})
export class CustomerStatusPipeModule { }
