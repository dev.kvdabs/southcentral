import { Pipe, PipeTransform } from '@angular/core';
import { HelperService } from '../../services/common/helper.service';
import { PAYMENT_METHOD } from 'src/app/constants/enums';

@Pipe({
  name: 'payment'
})
export class PaymentPipe implements PipeTransform {
  constructor(private helper: HelperService) {}

  transform(value: any, ...args: any[]): any {
    const types = this.helper.constToArray(PAYMENT_METHOD);
    const type = types.find(x => x.id == value);
    
    return type ? type.name : '';
  }

}
