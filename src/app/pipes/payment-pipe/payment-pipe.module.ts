import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaymentPipe } from './payment.pipe';

@NgModule({
  declarations: [PaymentPipe],
  imports: [
    CommonModule
  ],
  exports: [PaymentPipe]
})
export class PaymentPipeModule { }
