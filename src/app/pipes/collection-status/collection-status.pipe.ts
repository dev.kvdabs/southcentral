import { Pipe, PipeTransform } from '@angular/core';
import { HelperService } from '../../services/common/helper.service';
import { COLLECTION_STATUS } from 'src/app/constants/enums';

@Pipe({
  name: 'collectionstatus'
})
export class CollectionStatusPipe implements PipeTransform {
  constructor(private helper: HelperService) {}

  transform(value: any, ...args: any[]): any {
    const types = this.helper.constToArray(COLLECTION_STATUS);
    const type = types.find(x => x.id == value);
    
    return type ? type.name : '';
  }

}
