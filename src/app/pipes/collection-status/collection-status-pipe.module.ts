import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CollectionStatusPipe } from './collection-status.pipe';

@NgModule({
  declarations: [CollectionStatusPipe],
  imports: [
    CommonModule
  ],
  exports: [CollectionStatusPipe]
})
export class CollectionStatusPipeModule { }
