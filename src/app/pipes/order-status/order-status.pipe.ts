import { Pipe, PipeTransform } from '@angular/core';
import { HelperService } from '../../services/common/helper.service';
import { ORDER_STATUS } from 'src/app/constants/enums';

@Pipe({
  name: 'orderstatus'
})
export class OrderStatusPipe implements PipeTransform {
  constructor(private helper: HelperService) {}

  transform(value: any, ...args: any[]): any {
    const types = this.helper.constToArray(ORDER_STATUS);
    const type = types.find(x => x.id == value);
    
    return type ? type.name : '';
  }

}
