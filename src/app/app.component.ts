import { Component } from '@angular/core';

import { Platform, NavController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { AuthService } from './services/auth.service';
import { AppVersion } from '@ionic-native/app-version/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public appPages = [
    // {
    //   title: 'Home',
    //   url: '/home',
    //   icon: 'home'
    // },    
    {
      title: 'Customers',
      url: '/customers',
      icon: 'ios-people'
    },
    {
      title: 'Orders',
      url: '/orders',
      icon: 'ios-cart'
    },
    {
      title: 'Collections',
      url: '/collections',
      icon: 'ios-cash'
    },
    {
      title: 'Collection Plans',
      url: '/collection-plans',
      icon: 'ios-cash'
    }
  ];

  currentUserDisplayName: string;
  _appVersion = '1.0.0'

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private nav: NavController,
    private appVersion: AppVersion,
    private authService: AuthService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(async () => {
      //this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.authService.$currectUserDisplayName.subscribe(name => this.currentUserDisplayName = name);

      if (this.platform.is('cordova')) this._appVersion = await this.appVersion.getVersionNumber();
    });
  }

  goto(page) {    
    this.nav.navigateRoot(page.url);
  }

  logout() {
    setTimeout(() => this.authService.logout(), 1000);
    this.nav.navigateRoot('/login');
  }
}
