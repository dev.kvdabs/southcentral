import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectItemComponent } from './select-item.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [SelectItemComponent],
  entryComponents: [SelectItemComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ]
})
export class SelectItemModule { }
