import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { ItemService } from 'src/app/services/item.service';
import { HelperService } from 'src/app/services/common/helper.service';

@Component({
  selector: 'app-select-item',
  templateUrl: './select-item.component.html',
  styleUrls: ['./select-item.component.scss'],
})
export class SelectItemComponent implements OnInit {
  
  items: any;
  searchKey: string = '';
  selected: number[] = [];

  constructor(private selectItemModal: ModalController, 
              private params: NavParams, 
              private itemService: ItemService, 
              private helper: HelperService,
              private alertCtrl: AlertController) { }

  async ngOnInit() {

    this.selected = this.params.get('selected');

    try {
      this.items = await this.getItems()      
    } catch (error) {
      this.items = [];
    }
  }

  async getItems() {
    return await this.itemService.customerItems(this.params.get("customerId"));
  }

  async search(event) {
    if (event) {  
      const items = await this.getItems()
      this.items = this.helper.filter(items, "Name", event.target.value);
    }      
  }

  back(data?: any) {
    this.selectItemModal.dismiss(data);
  }

  async select(item: any) {
    const alert = await this.alertCtrl.create({
      header: 'Enter Quantity',      
      message: `${item.Name}`,
      mode: "ios",
      backdropDismiss: false,
      inputs: [
        {
          name: 'qty',
          type: 'number',
          placeholder: "1"
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            
          }
        },
        {
          text: 'Continue',
          handler: async (data) => {
            this.addToOrderItem(data, item);
          }
        }        
      ]
    });

    if (item.CustomerMinimumQty) alert.message += `<br><br>Cust Qty: ${item.CustomerMinimumQty}<br>Cust Price: ${item.CustomerPrice}`;

    alert.present();
  }

  addToOrderItem(data: any, item) {

    item.Qty = parseFloat(data.qty || 1);    
    let price = this.itemService.getPrice(item);
    const amount = item.Qty * price;

    let _item = {
      ItemID: item.ID,
      ItemName: item.Name,
      Qty: item.Qty,
      Price: price,
      Amount: amount,
      RequestedPrice: 0           
    }

    this.back(_item);
  }

  isSelected(itemId): boolean {
    return this.selected.some(x => x == itemId);
  }
}
