import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectTerritoryComponent } from './select-territory.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [SelectTerritoryComponent],
  entryComponents: [SelectTerritoryComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ]
})
export class SelectTerritoryModule { }
