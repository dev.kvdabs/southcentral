import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HelperService } from 'src/app/services/common/helper.service';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-select-territory',
  templateUrl: './select-territory.component.html',
  styleUrls: ['./select-territory.component.scss'],
})
export class SelectTerritoryComponent implements OnInit {
  
  territories: any;
  searchKey: string = '';

  constructor(private selectTerritoryModal: ModalController,               
              private helper: HelperService,
              private customerService: CustomerService) { }

  async ngOnInit() {
    try {
      this.territories = await this.getData()      
    } catch (error) {
      this.territories = [];
    }
  }

  async getData() {
    return await this.customerService.territories();
  }

  async search(event) {
    if (event) {  
      const territories = await this.getData()
      this.territories = this.helper.filter(territories, "Name", event.target.value);
    }
  }

  back(data?: any) {
    this.selectTerritoryModal.dismiss(data);
  }

  async select(territory: any) {
    this.back({
      id: territory.ID,
      name: territory.TerritoryName,
      area: {
        id: territory.AreaID,
        name: territory.AreaName
      }
    })
  }  
}
