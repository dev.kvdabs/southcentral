import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectCityComponent } from './select-city.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [SelectCityComponent],
  entryComponents: [SelectCityComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ]
})
export class SelectCityModule { }
