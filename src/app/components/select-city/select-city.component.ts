import { Component, OnInit } from '@angular/core';
import { ModalController, AlertController, NavParams } from '@ionic/angular';
import { HelperService } from 'src/app/services/common/helper.service';
import { CustomerService } from 'src/app/services/customer.service';

@Component({
  selector: 'app-select-city',
  templateUrl: './select-city.component.html',
  styleUrls: ['./select-city.component.scss'],
})
export class SelectCityComponent implements OnInit {
  
  cities: any;
  searchKey: string = '';
  showIntruction = true;

  constructor(private selectCityModal: ModalController, 
              private params: NavParams, 
              private helper: HelperService,
              private customerService: CustomerService,
              private alertCtrl: AlertController) { }

  async ngOnInit() {
    try {
      //this.cities = await this.getData()      
    } catch (error) {
      this.cities = [];
    }
  }

  async getData() {
    return await this.customerService.municipalities();
  }

  async search(event) {
    if (event && event.target.value.length > 2) {  
      this.showIntruction = false;
      const cities = await this.getData()
      this.cities = this.helper.filter(cities, ["Name", "ProvinceName"], event.target.value, 50);
    } else this.showIntruction = true;
  }

  back(data?: any) {
    this.selectCityModal.dismiss(data);
  }

  async select(city: any) {
    this.back({
      id: city.ID,
      name: city.Name,
      cityMunCode: city.CityMunCode,
      province: {
        id: city.ProvinceID,
        name: city.ProvinceName
      },
      region: {
        id: city.RegionID,
        name: city.RegionName
      }
    })
  }  
}
