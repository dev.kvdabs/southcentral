import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectInvoiceComponent } from './select-invoice.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';



@NgModule({
  declarations: [SelectInvoiceComponent],
  entryComponents: [SelectInvoiceComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ]
})
export class SelectInvoiceModule { }
