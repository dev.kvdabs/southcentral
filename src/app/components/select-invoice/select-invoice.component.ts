import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { CollectionService } from 'src/app/services/collection.service';

@Component({
  selector: 'app-select-invoice',
  templateUrl: './select-invoice.component.html',
  styleUrls: ['./select-invoice.component.scss'],
})
export class SelectInvoiceComponent implements OnInit {
  
  invoices: any[];
  collectionList: any[];
  selectedCount = 0;

  fromCollectionList: boolean;

  loading = true;

  constructor(private selectModal: ModalController, private collectionService: CollectionService, private params: NavParams) { }

  async ngOnInit() {
    try {
      this.invoices = await this.getInvoices();      
      
    } catch (error) {
      this.invoices = [];
    } finally {
      this.loading = false;
    }
  }

  async getInvoices() {
    return await this.collectionService.getCustomerInvoices(this.params.get("customerId"));
  }

  async getFromCollectionList() {
    return await this.collectionService.getCollectionListByCustomer(this.params.get("customerId"));
  }

  async getFrom(event) {
    if (event && event.detail.checked) {
      this.fromCollectionList = true;
      this.loading = true;

      try {
        this.collectionList = await this.getFromCollectionList();
        
      } catch (error) {
        this.collectionList = [];
      } finally {
        this.loading = false;
      }

    } else this.fromCollectionList = false;

    this.selectedCount = 0;
  }

  selectInvoice(invoice) {
    invoice.selected = !invoice.selected;

    this.selectedCount = this.invoices.filter(x => x.selected).length;
  }

  selectCollectionList(collection) {
    this.collectionList.forEach(x => x.selected = false);
    collection.selected = true;

    this.selectedCount = this.collectionList.filter(x => x.selected).length;
  }

  back(data?: any) {
    this.selectModal.dismiss(data);
  }

  selectInvoices() {
    const invoices = this.invoices.filter(x => x.selected);

    this.back(invoices);
  }

  selectCollection() {
    const collection = this.collectionList.find(x => x.selected);    
    this.back(collection);
  }
}
