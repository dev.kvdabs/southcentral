import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { PAYMENT_METHOD } from 'src/app/constants/enums';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {
  method: any;

  data: any;

  isCash: boolean;
  isCheque: boolean;
  isBankTransfer: boolean;
  isCredit: boolean;

  form: FormGroup;


  constructor(private selectModal: ModalController, 
              private fb: FormBuilder,
              private params: NavParams) { }

  ngOnInit() {
    this.data = this.params.get("data") || {};
    this.method = this.params.get("paymentMethod");
    if (this.method.id == PAYMENT_METHOD.CASH) this.isCash = true;
    else if (this.method.id == PAYMENT_METHOD.CHEQUE) this.isCheque = true;
    else if (this.method.id == PAYMENT_METHOD.BANK_TRANSFER) this.isBankTransfer = true;
    console.log(this.data)
    this.initForm(this.data);
  }

  initForm(object: any) {
    if (this.isCheque) {
      this.form = this.fb.group({
        ID: [0],
        CollectionID: [0],
        BankCode: ['', Validators.required],
        DueDate: [new Date().toISOString(), Validators.required],
        IsPostDated: [false],
        CheckNo: ['', Validators.required],
        Amount: ['', Validators.required],        
        Remarks: [''],
        Status: [4],
        RecordStatus: 0
      });
    } else {
      this.form = this.fb.group({
        ID: [0],
        CollectionID: [0],
        BankCode: ['', Validators.required],
        TransferDate: [new Date().toISOString(), Validators.required],
        ReferenceNo: ['', Validators.required],
        Amount: ['', Validators.required],
        Remarks: [''],
        Status: [4],
        RecordStatus: 0
      });
    }

    for (var key in object) {
      if (this.form.controls[key]) this.form.controls[key].setValue(object[key])
    }
  }

  back(data?: any) {
    this.selectModal.dismiss(data);
  }

  save() {
    if (!this.form.valid) return;
    this.back(this.form.value);
  }
}
