import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectCustomerComponent } from './select-customer.component';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [SelectCustomerComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule
  ],
  entryComponents: [SelectCustomerComponent]
})
export class SelectCustomerModule { }
