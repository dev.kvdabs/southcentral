import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CustomerService } from 'src/app/services/customer.service';
import { HelperService } from 'src/app/services/common/helper.service';

@Component({
  selector: 'app-select-customer',
  templateUrl: './select-customer.component.html',
  styleUrls: ['./select-customer.component.scss'],
})
export class SelectCustomerComponent implements OnInit {
  customers: any;

  constructor(private selectCustomerModal: ModalController, 
              private helper: HelperService,
              private customerService: CustomerService) { }

  async ngOnInit() {
    try {
      this.customers = await this.getCustomers();
      console.log(this.customers)
    } catch (error) {
      this.customers = [];
    }
  }

  async getCustomers() {
    return await this.customerService.customers(true);
  }

  async search(event) {
    if (event) {  
      const arr = await this.getCustomers()
      this.customers = this.helper.filter(arr, "Name", event.target.value);
    }      
  }

  back(data?: any) {
    this.selectCustomerModal.dismiss(data);
  }

  select(customer: any) {
    this.back({
      id: customer.ID,
      name: customer.Name
    });
  }
}
