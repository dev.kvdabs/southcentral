export const PAYMENT_METHOD = {
    CASH: 0,
    BANK_TRANSFER: 1,
    CHEQUE: 2
}

export const ORDER_TYPE = {
    DELIVERY: 0,
    SHIPMENT: 1,
    PICKUP: 2,
    WALKIN: 3
}

export const ORDER_STATUS = {
    Pending: 0,
    Declined: 1,
    Open: 2,
    Cancelled: 3,
    Released: 4,
    Picked: 5,
    Completed: 6,
    PartiallyReleased: 7,
    PartiallyPicked: 8,
    PartiallyDelivered: 9,
}

export const CUSTOMER_STATUS = {
    Prospect: 0,
    Verified: 1
}

export const COLLECTION_STATUS = {
    Prospect: 0,
    Verified: 1
}